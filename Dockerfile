# Dependencies Storage --------------------------------------------------------
FROM node:18.10.0-alpine AS dependencies-storage

RUN apk -U add --no-cache libc6-compat

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install --frozen-lockfile

# Builder ---------------------------------------------------------------------
FROM node:18.10.0-alpine AS builder

WORKDIR /app
COPY --from=dependencies-storage /app/node_modules ./node_modules
COPY . .

RUN npm run build

# Runner ----------------------------------------------------------------------
FROM node:18.10.0-alpine AS runner

WORKDIR /app

COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/next.config.js ./next.config.js
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/package-lock.json ./package-lock.json

# COPY --from=builder /app/.next/standalone ./
# COPY --from=builder /app/.next/static ./.next/static

# COPY --from=builder /app/next.config.js ./

RUN addgroup -S stolen-bikes-challenge && \
    adduser -S stolen-bikes-challenge -G stolen-bikes-challenge && \
    chown -R stolen-bikes-challenge:stolen-bikes-challenge /app

USER stolen-bikes-challenge

# ARG PORT 3000
# ENV PORT $PORT
ENV PORT 3000
ENV HOSTNAME 0.0.0.0

CMD ["npm", "run", "start"]
