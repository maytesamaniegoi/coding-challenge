/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'files.bikeindex.org',
      },
      {
        protocol: 'https',
        hostname: 'bikebook.s3.amazonaws.com'
      },
      {
        protocol: 'https',
        hostname: 'mayte.samaniego.me'
      },
    ],
  },
  env: {
    APP_URL_API: 'https://bikeindex.org:443/api/v3'
  }
}

module.exports = nextConfig
