const EmptyCard = () => {
    return (
        <div className="flex w-full justify-center pt-10">
            <div className="flex flex-col items-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-20 h-20">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636" />
                </svg>
                <span className="font-semibold">
                0 results founded for this search within 200 miles of Munich.
                </span>
            </div>
        </div>
    )
}

export default EmptyCard