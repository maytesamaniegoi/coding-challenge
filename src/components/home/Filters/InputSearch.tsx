import { ChangeEventHandler, useState, KeyboardEventHandler } from "react"
import { useRouter, useSearchParams } from 'next/navigation'

import {useCreateQueryString} from '@/utils/hooks'

type InputSearchProps = {
    placeholder?: string,
    className?: string,
    allowPressEnter?: boolean
}

const InputSearch = ({ placeholder = 'Search by name', className, allowPressEnter = false }: InputSearchProps) => {
    const router = useRouter()
    const searchParams = useSearchParams()
    const queryDefaultValue = searchParams.get('query')
    const [value, setValue] = useState<string>(queryDefaultValue || '')
    const getQueryParameters = useCreateQueryString()

    const _handleChangeQuery: ChangeEventHandler<HTMLInputElement> = (e) => {
        e.preventDefault()
        const {value} = e.target
        setValue(value)
    }

    const _handleKeyPress: KeyboardEventHandler<HTMLInputElement>  = (e) => {
        if(e.code === 'Enter'){
            const query = getQueryParameters('query', value)
            router.push('?' + query)
        }
    }

    return (
        <input 
        className={`input input-solid ${className || ''}`}
        onChange={_handleChangeQuery}
        defaultValue={queryDefaultValue || ''}
        placeholder={placeholder}
        {
            ...allowPressEnter ? {
                onKeyPress: _handleKeyPress
            } : {}
        }
        />
    )
}

export default InputSearch