import React, { useState } from "react";
import { useRouter, useSearchParams } from 'next/navigation'

import Datepicker, { DateValueType } from "react-tailwindcss-datepicker";

import {useCreateQueryString} from '@/utils/hooks'

const RangeDatePicker = () => {
    const router = useRouter()
    const searchParams = useSearchParams()
    const getQueryParameters = useCreateQueryString()

    const [value, setValue] = useState<DateValueType>(() => ({
        startDate: searchParams.get('startDate') || new Date(),
        endDate: searchParams.get('endDate') ||new Date()
    }));
    

    const handleValueChange = (newValue: DateValueType) => {
        setValue(newValue);
        getQueryParameters('startDate', String(newValue?.startDate) || '')
        const endDateQuery = getQueryParameters('endDate', String(newValue?.endDate) || '')
        router.push(`?${endDateQuery}`)
    };

    return (
        <Datepicker 
        value={value} 
        dateLooking='middle'
        useRange={false}
        toggleIcon={() => <></>}
        showFooter
        containerClassName={'datepicker-container'}
        onChange={handleValueChange} 
        maxDate={new Date()} 
        /> 
    );
}

export default RangeDatePicker