import RangeDatePicker from '@/components/home/Filters/RangeDatePicker'
import InputSearch from '@/components/home/Filters/InputSearch'

import {useScreen} from '@/utils/hooks'

const filtersData = [
    {
        id: 'filter-name',
        title: 'Filter by name',
        icon: <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
        <path strokeLinecap="round" strokeLinejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
      </svg>,
        type: 'input',
        placeholder: 'Search by name'
      
    },
    {
        id: 'filter-date',
        title: 'Filter by stolen date',
        icon: <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
        <path strokeLinecap="round" strokeLinejoin="round" d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z" />
      </svg>,
      type: 'calendar'                              
    }
]

const Filters = () => {
    const {isMobile} = useScreen()

    return (
        <ul className="menu-items">
            {
                filtersData.filter(e => !isMobile ? e.id!=='filter-name': true).map(({ id, title, icon, type, placeholder }) => (
                    <li key={id}>
                        <div className="flex items-center gap-2">
                            {icon}
                            <span>{title}</span>
                        </div>

                        <div className="">
                            <div className="min-h-0 py-3">
                                {
                                    type === 'calendar' ? (
                                        <RangeDatePicker />
                                    ) : type === 'input' ? (
                                        <InputSearch allowPressEnter className='w-full' />
                                    ) : null
                                }
                            </div>
                        </div>
                    </li>
                ))
            }
        </ul>
    )
}

export default Filters