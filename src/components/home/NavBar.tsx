import React from 'react'
import Filters from '@/components/home/Filters'

const NavBar = ({
    children,
}: {
    children: React.ReactNode,
}) => {
    return (
    <div className="navbar">
        <div className="navbar-start">
            <a className="navbar-item flex items-center"> 
                <svg fill="none" height="42" viewBox="0 0 32 32" width="42" xmlns="http://www.w3.org/2000/svg">
                    <rect height="100%" rx="16" width="100%"></rect>
                    <path clipRule="evenodd" d="M17.6482 10.1305L15.8785 7.02583L7.02979 22.5499H10.5278L17.6482 10.1305ZM19.8798 14.0457L18.11 17.1983L19.394 19.4511H16.8453L15.1056 22.5499H24.7272L19.8798 14.0457Z" fill="currentColor" fillRule="evenodd"></path>
                </svg>
                <div className="flex flex-col">
                    <span>Stolen Bikes in München</span>
                </div>
            </a>
	    </div>
        <div className="navbar-center">
            {children}
        </div>
        <div className="navbar-end">
        <div className="popover">
            <label className="popover-trigger my-2 btn btn-solid-primary" tabIndex={0}>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 6h9.75M10.5 6a1.5 1.5 0 11-3 0m3 0a1.5 1.5 0 10-3 0M3.75 6H7.5m3 12h9.75m-9.75 0a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m-3.75 0H7.5m9-6h3.75m-3.75 0a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m-9.75 0h9.75" />
            </svg>
            </label>
            <div className="popover-content popover-bottom-left w-96" tabIndex={0}>
                <div className="popover-arrow"></div>
                <div className="p-4 text-sm">
                    <Filters />
                </div>
            </div>
        </div>
        </div>
    </div>
    )
}

export default NavBar   