'use client'
import { useCallback, useEffect, useState } from 'react'
import { useSearchParams } from 'next/navigation'

import EmptyCard from '@/components/common/EmptyCard'
import Loading from '@/components/common/Loading'

import SimpleCard from '@/components/home/SimpleCard'
import Pagination from '@/components/home/Pagination'

import {BikeType, Paginationtype} from '@/utils/types'
import {getQueryParameters} from '@/utils'

const defaultPagination = {
  total: 0,
  limit: 0,
  pages: 0,
  currentPage: 0
}

const MainView = () => {
    const searchParams = useSearchParams()
  
    const [loading, setLoading] = useState<boolean | null>(null)
    const [dataBikes, setDataBikes] = useState<Array<BikeType>>([])
    const [pagination, setPagination] = useState<Paginationtype>(defaultPagination)
  
    const getStolenBikes = useCallback((searchParams: URLSearchParams) => {
      setLoading(true)
      const limit = searchParams.get('limit')
      const currentPage = searchParams.get('currentPage')
      const query = searchParams.get('query')
      const startDate = searchParams.get('startDate')
      const endDate = searchParams.get('endDate')
      
      fetch(
        `/api/bikes?${
            getQueryParameters({
                limit, 
                page: currentPage, 
                query,
                startDate,
                endDate
            })
        }`,
      ).then((res: Response) => {
        return res.json()
      }).then((res) => {
        setDataBikes(res.data)
        setPagination(res.pagination || defaultPagination)
      }).catch((e) => {
        console.error('Error:', e)
      }).finally(() => {
        setLoading(false)
      });
    }, [])
  
    useEffect(()=> {
      getStolenBikes(searchParams)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchParams])
  
    if(loading === null) return null

    return(
        <>
      {
        pagination.total === 0 && loading === false ? ( 
          <EmptyCard />
        ) : (
          <div className='p-6'>
          {
            !loading? ( 
            <span> There are {pagination.total} stolen bicycles for this search within 200 miles of Munich</span>
            ) :  <Loading />
          }
          
          <div className={[
            'mt-5',
            'xs:flex xs:flex-col xs:items-center', 
            'sm:gap-3 sm:grid-cols-3 sm:grid', 
            'md:grid-cols-4 md:gap-4'
          ].join(' ')}>
            {
              !loading ? dataBikes?.map(({id, ...props}: BikeType) => (
                <div className='h-full w-full max-xs:mb-4' key={id}>
                  <SimpleCard id={id} {...props} />
                </div>
              )) : (
                Array.from({length: 10}, (_, i) => (
                  <div className='h-full w-full max-xs:mb-4' key={i+1}>
                    <div className="skeleton-static rounded-xl h-96" ></div>
                  </div>
                ))
              )
            }
          </div>
          {
            loading ? null : (
                <div className='flex justify-center w-full mt-4'>
                    <Pagination {...pagination} />
                </div>
            )
          }
        </div>
        )
      }
      </>
    )
}

export default MainView