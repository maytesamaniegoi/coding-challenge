import Image from 'next/image'
import {BikeType} from '@/utils/types'
import moment from 'moment'

const details = [
    {
        key: 'serial',
        label: 'Serial',
        getValue: (value?: string | number) => String(value)
    },
    {
        key: 'frameColors',
        label: 'Colors',
        getValue: (value?: string[]) => value && value.length ? value.join(',') : 'Unknown'
    },
    {
        key: 'dateStolen',
        label: 'Stolen date',
        getValue: (date?: number | undefined) =>  date ? moment(isNaN(Number(date)) ? date : moment.unix(date).utc()).format('DD.MM.YYYY HH:mm') : 'Unknown'
    },
    {
        key: 'stolenLocation',
        label: 'Stolen location',
        getValue: (value?: string | undefined) => value || 'Unknown'
    }
]
type DetailsType = {
    key: string,
    label: string,
    getValue: Function
}

const SimpleCard : any = ({ 
    title = '', 
    large_img: imageUrl ,
    description,
    ...props
}: BikeType) => {
    const values : {[key: string]: any} = {
        serial: props.serial,
        frameColors: props.frame_colors,
        stolenLocation: props.stolen_location,
        dateStolen: props.date_stolen
    }
    
    return (
        <div className="card card-image-cover h-full">
            {
                imageUrl ? (
                    <Image 
                    className='w-full max-h-64'
                    width={300}
                    height={300}
                    alt={"frame"+ title} src={imageUrl} />
                ) : (
                    <Image 
                    className='w-full h-64'
                    width={300}
                    height={300}
                    alt={"frame"+ title} src={'/not-found-image.webp'} />
                )
            }
            
            <div className="card-body">
                <h2 className="card-header">{title}</h2>
                <div className="text-content2 flex flex-col">
                    {description ? <span className='mb-2 hidden-text-3-lines'>{description}</span>:null}
                    {
                        details.map(({key, label, getValue}: DetailsType) => {
                            return (
                            <div key={key}>
                                <span className='mr-2 font-semibold'>{label}:</span>
                                <span>{getValue(values[key])}</span>
                            </div>
                        )})
                    }
                </div>
                {/* <div className="card-footer">
                    <button className="btn-primary btn">See details</button>
                </div> */}
            </div>
        </div>
    )
}

export default SimpleCard