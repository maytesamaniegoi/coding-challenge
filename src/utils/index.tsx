export const getQueryParameters = (params: {[key: string]: any}) => {
    let queryResult = []
    for(let key in params){
      if(params[key]){
        queryResult.push(`${key}=${params[key]}`)
      }
    }
    return queryResult.join('&')
};