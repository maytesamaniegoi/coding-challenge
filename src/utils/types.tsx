export type BikeType = {
    id: string | number , 
    title: string | any,
    date_stolen: Date | number | any,
    description: string | any,
    large_img: string | any,
    location_found: string | any,
    url: string | any,
    year: number | any,
    stolen_location: string | any,
    status: string | any,
    stolen: boolean,
    serial: string | number,
    frame_colors: Array<string>
}

export type Paginationtype = {
    currentPage: number,
    total: number,
    limit: number,
    pages: number
}

export type FilterDateType = {
    startDate?: string | null,
    endDate?: string | null
}

export type BikesPaginationType = {
    data: Array<BikeType>,
    pagination: Paginationtype
}