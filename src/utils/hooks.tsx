import { useEffect, useState } from 'react'
import { useSearchParams } from 'next/navigation'

export const useCreateQueryString = () => {
    const searchParams = useSearchParams()

    const params = new URLSearchParams(searchParams)

    return (name: string, value: string | number) => {
        params.set(name, String(value))
        return params.toString()
    }
}

export const useScreen =() => {
    const [screenWidth, setScreenWidth] = useState<number>(0)

    useEffect(() => {  
        const updateScreen = () => setScreenWidth(window.innerWidth)
        updateScreen()
        window.addEventListener('resize', updateScreen)
        return () => window.removeEventListener('resize', updateScreen)
    }, [])

    return {
        isMobile: screenWidth < 475
    }
}