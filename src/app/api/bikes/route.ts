import { NextRequest, NextResponse } from 'next/server'
import {getQueryParameters} from '@/utils'
import { BikeType, FilterDateType, BikesPaginationType } from '@/utils/types'
import moment from 'moment'

type BikesCount = { non: number, stolen: number, proximity: number }

const getTotalBikes = async (querySearch: string) => {
  const res = await fetch(`${process.env.APP_URL_API}/search/count?${querySearch}`, {
    headers: {
      'Content-Type': 'application/json',
    },
  }) 
  const data: BikesCount = await res.json()

  return data.proximity
}

const getDataPagination = (data: Array<BikeType>, {currentPage, limit} : {currentPage: number, limit: number}) : BikesPaginationType => {
  const total = data.length
  const dataResult = data.slice((currentPage - 1)*limit, currentPage*limit)

  return {
    data: dataResult,
    pagination: {
      limit,
      currentPage,
      pages: Math.ceil(total/limit),
      total
    }
  }
}

const applyingRangeDateFilter = (list: Array<BikeType>, filters: FilterDateType) : Array<BikeType>=> {
  return list.filter((bike) => {
    let res = true
    if(filters.startDate){
      const diff = moment(moment.unix(bike.date_stolen).utc()).diff(moment(filters.startDate))
      res = res && diff >= 0
    }
    if(res && filters.endDate){
      const diffEnd = moment(moment(filters.endDate)).diff(moment.unix(bike.date_stolen).utc())
      res = res && diffEnd >= 0
    }
    return res
  })
}

export const GET = async (request:  NextRequest) => {
  const page_str = request.nextUrl.searchParams.get("page");
  const limit_str = request.nextUrl.searchParams.get("limit");
  const query_str = request.nextUrl.searchParams.get("query");
  const start_date_str = request.nextUrl.searchParams.get("startDate");
  const end_date_str = request.nextUrl.searchParams.get("endDate");

  const currentPage = page_str ? parseInt(page_str, 10) : 1;
  const limit = limit_str ? parseInt(limit_str, 10) : 10;

  const querySearch = getQueryParameters({
    // 'per_page': limit, 
    // page: currentPage, 
    'query_items%5B%5D': query_str,
    location: "munich",
    distance: 200,
    stolenness: 'proximity'
  })

  const totalBikes = await getTotalBikes(querySearch)

  // Making batch of 100 per_page for pagination, because of api source limitations
  const batchSize = 100
  const batchs = Math.ceil(totalBikes/batchSize)

  let result: any[] = []
  for(let i = 0; i < batchs; i ++){
    const res = await fetch(process.env.APP_URL_API + `/search?${querySearch}&per_page=${batchSize}&page=${i + 1}`,
    {
     headers: {
       'Content-Type': 'application/json',
     },
   })
   
    const resParsed = await res.json()
    result = result.concat(resParsed.bikes)
  }
  const bikesResult = Boolean(start_date_str && end_date_str) ? applyingRangeDateFilter(result, {
    startDate: start_date_str, 
    endDate: end_date_str
  }) : result
 
  return NextResponse.json(getDataPagination(bikesResult, {currentPage, limit}))
}

export const POST = async() => {}