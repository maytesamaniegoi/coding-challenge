'use client'
import { Suspense } from 'react'

import Loading from '@/components/common/Loading'
import MainView from '@/components/home/MainView'
import NavBar from '@/components/home/NavBar'
import InputSearch from '@/components/home/Filters/InputSearch'

import {useScreen} from '@/utils/hooks'

const Home = () => {
  const {isMobile} = useScreen()

  return (
    <main>
      <Suspense fallback={<Loading />}>
          <NavBar>
          {!isMobile ? (
            <InputSearch 
            allowPressEnter 
            className='w-96' 
            placeholder='Press enter to search' /> 
          ): null} 
          </NavBar>
          <MainView />
      </Suspense>
    </main>
  )
}

export default Home