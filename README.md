# Stolen Bike Index - Carly Coding Challenge

This repository contains a Server-side application built with Next.js. 

The application is available in the following link: https://mayte.samaniego.me/ 

Achieved Objectives:

- [ ] I want to see a list of reported bike thefts for the Munich area.
- [ ] I want to see the first 10 bike theft cases, with the ability to - paginate (10 cases per page).
- [ ] I want to see a total number of bike theft cases.
- [ ] For each reported bike theft I want to see:
  - [ ] Case title
  - [ ] Case description
  - [ ] Date of the theft
  - [ ] Date of when the case was reported
  - [ ] Location of the theft
  - [ ] Picture of the bike, if available
- [ ] I want to filter reported bike thefts by partial case title.
- [ ] I want to filter reported bike thefts by date range.
- [ ] I want to see a loading state until the list is available.
- [ ] I want to see an error state if the list is unavailable.
- [ ] I want to see an empty state if there are no results.

## Tech Requirements

This application covers the following tech requirements:

- React
- Code Linter
- Typescript 
- Css Tailwind

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
