import type { Config } from 'tailwindcss'
import defaultTheme  from 'tailwindcss/defaultTheme'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js",
  ],
  theme: {
    // mode: false,
    screens: { 
      'xs': '475px',
      ...defaultTheme.screens,
    },
    colors: {
      ...defaultTheme.colors,
      'blue': {
        100: '#9abcf2',
        200: '#72a1ed',
        300: '#538ce9',
        400: '#306cce',
        500: '#1976d2',
        600: '#2554a0'
      },
    },
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
  },
  rippleui: {
    removeThemes: ["dark"],
    themes: [
      {
				themeName: "light",
				colorScheme: "light",
				colors: {
					primary: "#1976d2",
					backgroundPrimary: "#D6DBDC",
				},
			},
    ]
  },
  plugins: [require("rippleui")],
}
export default config
